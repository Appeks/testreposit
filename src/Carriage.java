public class Carriage {
    boolean isLightOn;

    public Carriage(boolean isLightOn) {
        this.isLightOn = isLightOn;
    }

    public void setLightOn(boolean lightOn) {
        isLightOn = lightOn;
    }

    public boolean isLightOn() {
        return isLightOn;
    }
}


