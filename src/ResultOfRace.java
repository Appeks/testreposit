public class ResultOfRace {
    public static void main(String[] args) {
        String[] names = { "Elena", "Thomas", "Hamilton", "Suzie", "Phil", "Matt", "Alex", "Emma", "John", "James", "Jane", "Emily", "Daniel", "Neda", "Aaron", "Kate" };
        int[] times = { 341, 273, 278, 329, 445, 402, 388, 275, 243, 334, 412, 393, 299, 343, 317, 265 };
        int minimum = times[0];
        byte number = 0;
        for (byte i = 1; i < times.length; i++) {
            if (times[i] < minimum)
            {
                minimum = times[i];
                number = i;
            }
        }
        System.out.println("Победитель - " + names[number] + " со временем " + minimum + " секунды");
    }
}
