import java.util.*;

class Solution {
    private byte countDigits(int a) {
        byte count = 0;
        if (a == 0) count = 1;
        while (a != 0){
            count++;
            a /= 10;
        }
        return count;
    }

    public int solution(int A, int B) {
        // write your code in Java SE 8
        int c, tempA, tempB, tempC = 0;
        int pow = 1;
        byte countA = 0;
        byte countB = 0;
        countA = countDigits(A);
        countB = countDigits(B);
        while (countA!= 0) {
            if (countA > countB) {
                tempC+= (A % 10)*pow;
                A /= 10;
                countA--;
            } else {
                tempC+= (B % 10)*pow;
                B /= 10;
                countB--;
            }
            pow *= 10;
        }
        if (tempC > 100_000_000) c = -1;
        else c = tempC;
        return c;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.solution(1004, 2));
    }

}