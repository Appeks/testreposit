import java.util.Random;

public class LinkedCarriage {
    private LinkedCarriage next;
    private LinkedCarriage previos;
    private boolean isLightOn;
    private Random random = new Random();
    private int number;

    public LinkedCarriage(LinkedCarriage next, LinkedCarriage previos, int number) {
        this.next = next;
        this.previos = previos;
        this.number = number;
        this.isLightOn = random.nextBoolean();
    }

    public LinkedCarriage getNext() {
        return next;
    }

    public void setNext(LinkedCarriage next) {
        this.next = next;
    }

    public LinkedCarriage getPrevios() {
        return previos;
    }

    public boolean isLightOn() {
        return isLightOn;
    }

    public void setLightOn(boolean lightOn) {
        isLightOn = lightOn;
    }

    public void setPrevios(LinkedCarriage previos) {
        this.previos = previos;
    }

    public int getNumber() {
        return number;
    }

    public void printTrainFromHere() {
        LinkedCarriage current = this;
        do {
            System.out.println(current.toString());
            current = current.getNext();
        } while (current != null && current != this);
    }

    @Override
    public String toString() {
        return String.valueOf(getNumber());
    }
}
