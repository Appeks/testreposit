
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MiddleDouble  {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите 10 чисел Double: ");
        double[] dbl = new double[10];
        double result = 0;
        for (int  i = 0;  i < 10;  i++) {
            dbl[i] = Double.parseDouble(reader.readLine());
            result+= dbl[i];
        }
        System.out.printf("Среднее арифметическое введенных значений: %6.2f", result/10.0);
    }
}
