public class Main2 {

    private static final int TRAIN_LENGTH = 6;

    public static void main(String[] args) {
        LinkedCarriage head = initTrain();
        System.out.println("Carriages numbers: ");
        head.printTrainFromHere();
        LinkedCarriage headReverse = getReverseTrain(head);
        System.out.println("Carriages reverse numbers: ");
        headReverse.printTrainFromHere();
        System.out.println("length is : " + getTrainLengthFromHead(head));
        System.out.println("Length with reverse numbers is: " + getTrainLengthFromHead(headReverse));
        System.out.println("Length in 2-direction method: " + getLengthInTwoWays(head));
    }

    private static LinkedCarriage initTrain() {
        LinkedCarriage head = new LinkedCarriage(null, null, 1);
        LinkedCarriage current = new LinkedCarriage(null, head, 2);

        head.setNext(current);
        current.setPrevios(head);

        LinkedCarriage previous = current;

        for (int i = 0; i < TRAIN_LENGTH - 2; i++) {
            current = new LinkedCarriage(null, previous, i + 3);
            previous.setNext(current);
            previous = current;
        }

        current.setNext(head);
        head.setPrevios(current);

        return head;
    }

    private static int getTrainLengthFromHead(LinkedCarriage head) {
        int length = 0;
        if (head == null) {
            return 0;
        } else {
            length++;
        }
        LinkedCarriage current = head.getNext();
        while (current != null && current != head) {
            current = current.getNext();
            length++;
        }
        return length;
    }

    private static int getLengthInTwoWays(LinkedCarriage head) {
        int lengthUp = 0, lengthDown = 0;
        if (head == null) {
            return 0;
        } else {
            lengthUp++;
            lengthDown++;
        }
        LinkedCarriage currentUp = head.getNext();
        LinkedCarriage currentDown = head.getPrevios();
        while (((currentUp != null) && (currentDown != null)) && (currentUp != currentDown)) {
            currentUp = currentUp.getNext();
            if (currentUp == currentDown) {
                lengthUp++;
                break;
            }
            currentDown = currentDown.getPrevios();
            lengthUp++;
            lengthDown++;
        }
        if (currentDown != currentUp) System.out.println("Error in list");
        return lengthUp + lengthDown;
    }

    private static LinkedCarriage getReverseTrain(LinkedCarriage head) {
        LinkedCarriage current = head.getNext();
        head.setPrevios(current);
        LinkedCarriage next;
        LinkedCarriage previos = head;
        while (current != head) {
            next = current.getNext();
            current.setPrevios(next);
            current.setNext(previos);
            previos = current;
            current = next;
        }
        head.setNext(previos);
        return previos;
    }
}
