import java.util.TreeSet;

public class SimpleNumbers {
    public static void main(String[] args) {
        TreeSet<Integer> set = new TreeSet<>();
        boolean isNotSimple;
        set.add(2);
        for (int i = 3; i < 100; i++) {
            isNotSimple = false;
            for (Integer element: set) {
                if (i%element == 0) {
                    isNotSimple = true;
                    break;
                }
            }
            if (!isNotSimple) set.add(i);
        }
        System.out.println("Простые числа от 1 до 100: ");
        for (Integer element: set)
        {
            System.out.print(element + " ");
        }
        System.out.println();
        System.out.println("Составные числа от 1 до 100: ");
        for (int i = 2; i < 100; i++) {
            if (!set.contains(i)) System.out.print(i + " ");
        }
    }
}
