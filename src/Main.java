import java.util.LinkedList;

public class Main {
    private static final int COUNT = 6;


    public static void main(String[] args) {
        System.out.println("Высчитанное вручную число вагонов: " + countCarriage());
        System.out.println("Реальное число вагонов: " + COUNT);
    }

    private static int countCarriage() {
    int i = 0, count = 0;
    boolean isFirstReached = false;
    LinkedCarriage head = initializeTrain();
    head.setLightOn(false);
    LinkedCarriage current = head;
    while (!isFirstReached){
         do {
             current = current.getNext();
             count++;
         } while (current.isLightOn());
         current.setLightOn(true);
         if (head.isLightOn()) isFirstReached = true;
         else {
             count = 0;
             current = head;
         }
        }
    return count;
    }

    public static LinkedCarriage initializeTrain(){
        LinkedCarriage head = new LinkedCarriage(null, null, 1);
        LinkedCarriage current = null;
        LinkedCarriage previos = head;
        for (int i = 1; i < COUNT ; i++) {
            current = new LinkedCarriage(null, previos, i);
            previos.setNext(current);
            previos = current;
        }
        head.setPrevios(current);
        current.setNext(head);
        return head;
    }
}
