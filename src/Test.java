import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Укажите загаданную букву");
        String s = reader.readLine();
        Character ch;
        Character c = s.charAt(0);
        boolean success = false;
        while (!success) {
            System.out.println("Угадайте букву");
            s = reader.readLine();
            ch = s.charAt(0);
            if (c.equals(ch)) success = true;
            if (ch.compareTo(c) < 0) System.out.println("Меньше загаданной");
            else if (ch.compareTo(c) > 0) System.out.println("Больше загаданной");
        }
        System.out.println("Поздравляем!");
    }
}
